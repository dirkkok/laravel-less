<?php

namespace Dirkkok\LaravelLess;

use lessc;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

/**
 * Class Less
 *
 * @package Dirkkok\LaravelLess
 */
class Less
{
    protected $lesssc;

    protected $inputFiles = [];

    protected $outputFile;

    public function __construct(lessc $lesssc)
    {
        $this->lesssc = $lesssc;
    }

    /**
     * @param array $inputFiles Paths to css or less files (relative to public_path())
     * @param $outputFile Path in you public folder to write the compiled css to (path relative to public_path())
     * @return string
     * @throws FileNotFoundException
     */
    public function compile($inputFiles = [], $outputFile)
    {
        $this->outputFile = public_path($outputFile);

        $hash = md5(serialize($inputFiles));

        $cached = $this->getCached($hash);

        if ($cached and File::exists($this->outputFile)) {
//            return asset($outputFile);
            return asset($outputFile) . '?t=' . $cached;
        }

        $this->addFiles($inputFiles);

        $compileString = $this->buildCompileString();

        $this->lesssc->setFormatter("compressed");
        $compiledCss = $this->lesssc->compile($compileString);

//        $parser = new \Less_Parser(['compress' => true]);
//        $parser->parse($compileString);
//        $compiledCss = $parser->getCss();

        File::put($this->outputFile, $compiledCss);

        $this->updateCache($hash);

        return asset($outputFile);
    }

    /**
     * @param array $inputFiles
     *
     * @throws FileNotFoundException
     */
    protected function addFiles($inputFiles = [])
    {
        foreach($inputFiles as $inputFile) {
            if (! File::exists(public_path($inputFile))) {
                throw new FileNotFoundException("File does not exist at path {$inputFile}");
            }

            $this->inputFiles[] = public_path($inputFile);
        }
    }

    /**
     * @return string
     */
    protected function buildCompileString()
    {
        $lessStrings = [];

        foreach($this->inputFiles as $file) {
            $lessStrings[] = strpos($file, '.css') === false ? '@import "' . $file . '";' : '@import (inline) "' . $file . '";';
        }

        return implode(' ', $lessStrings);
    }

    /**
     * @param $hash
     *
     * @return mixed
     */
    protected function getCached($hash)
    {
        $cacheKey = 'laravel-less-' . $hash;

        if (! Cache::has($cacheKey)) {
            return false;
        }

        return Cache::get($cacheKey);
    }

    /**
     * @param $hash
     */
    protected function updateCache($hash)
    {
        $cacheKey = 'laravel-less-' . $hash;

        Cache::forever($cacheKey, time());
    }
}

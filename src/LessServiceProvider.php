<?php

namespace Dirkkok\LaravelLess;

use Illuminate\Support\ServiceProvider;

/**
 * Class LessServiceProvider
 *
 * @package Dirkkok\LaravelLess
 */
class LessServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('laravel.less', function ()
        {
            return new Less(new \lessc());
        });
    }
}

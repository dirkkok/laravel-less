<?php

namespace Dirkkok\LaravelLess;

use Illuminate\Support\Facades\Facade;

/**
 * Class LessFacade
 *
 * @package Dirkkok\LaravelLess
 */
class LessFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel.less';
    }
}
